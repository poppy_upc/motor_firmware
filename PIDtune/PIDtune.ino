#define ADDR    0x01
#define IN_A 4
#define IN_B A3



int offset = 0;

/*          Control loop            */
boolean control_loop = false;

/*        Magnetic encoder          */
uint8_t u8byteCount;
uint8_t u8data;
uint32_t u32result = 0;
uint32_t u32send;

// Position (feedback) variables
int pos;
int prev_pos;

// Velocity (feedback) variables
int vel;
int velocity;
float vf;   // -v-elocity -f-iltered
float prev_vf;
float prev_vf1 = 0;
float prev_vf2 = 0;
float prev_vf3 = 0;
float v_rad, vf_rad;

/*    --- Control variables ---   */
// Goal position
int goal_position;
int vel_reference;
int prev_vel;
int goal_tolerance = 5; // in ticks
int e_pos = 0;
int prev_e_pos = 0;

float ref, e, prev_e;
float uP, uI, uD;
float u;
float Kp, Ki, Kd;   // capital K
float pwm;
uint8_t duty8;
int16_t duty16;

// Filter variables
float a = 0.1484;
float b = 0.8516;
// Lin/son variables
float af =  9.347;
float bf = 0.04626;
float cf = 0.00363;
float df = 0.3661;
//
boolean flag = false;;
boolean timeoutRX = false;

int loop_cnt = 0;
bool dir = 1;



/* Counter2 compare match interrupt - for control loop*/
ISR(TIMER2_COMPA_vect, ISR_NOBLOCK) {
  //  delayMicroseconds(70);  // read sensor time
  if (control_loop == 0) {
    position_update(pos);
    control_loop = 1;
  }
}


void setup() {
  pinMode(10, OUTPUT); // SPI pulse
  pinMode(4, OUTPUT);  // Direction pin
  pinMode(A3, OUTPUT);  // Direction pin
  pinMode(11, OUTPUT); // OC2A
  pinMode(5, OUTPUT);  // PWM
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  /* --------------------  Serial Iinit.  --------------------- */
  Serial.begin(115200);

  /* ----------  Timer 2 (control loop) configuration  -------- */
  TCCR2A = 0;
  TCCR2B = 0;    //DON'T KNOW WHY - Init. Value of reg = B00000100 (!!)

  TCCR2A |= B00000010;
  TCCR2B |= B00000111;
  // DONT FORGET
  TIMSK2 |= B00000010;
  control_loop = 1;
  //  OCR0A = 155; // 14 = 1ms, 155 = 10ms
  OCR2A = 96; // (prescaler:111 -> 4.983ms period

  /* ---------- Timer 0 - Configuration (FAST_PWM) ------------ */
  TCCR0A = 0;
  TCCR0B = 0;    //DON'T WHY - Init. Value of reg = B00000100 (!!)
  TCCR0A |= B00100011;
  TCCR0B |= B00000010;  // 9.8Khz (bridge works up to 20 MHz BUT...
  /* -----------------------------------------------------------*/


  /* ------------------  SPI configuration  ------------------- */
  DDRB = 0;  //Check if needed
  // configure SCK(PB5) and Slave Select(PB2) as output, MISO(PB4) as input
  DDRB = (1 << PB5) | (1 << PB2) | (0 << PB4);
  // configure SPI as master, SPR0=1 -> fosc/16 CHANGE: SPR0 = 0 -> fosc/4
  // 16 Mhz XTAL:
  //  SPCR = (1 << SPE) | (1 << MSTR) | (0 << CPOL) | (1 << SPR0) | (CPHA << 1);
  // 20 Mhz XTAL:
  SPCR = (1 << SPE) | (1 << MSTR) | (0 << CPOL) | (1 << SPR1) | (CPHA << 1);



  digitalWrite(IN_B, LOW);
  digitalWrite(IN_A, HIGH);
  OCR0B = 0;

  ref = 0.0;
  Kp = 1.0;
  Ki = 0.0;
  Kd = 0.0;
  goal_position = 2048;
  
  sei();
}




void loop() {
  if (control_loop == true) {
    // New control loop

    // Position already updated inside ISR!

    // Velocity
    vel = velocity_update(pos, prev_pos); // noisy signal
    vf = a * prev_vel + b * prev_vf;
    vf = (vf + prev_vf + prev_vf1 + prev_vf2) / 4;
    velocity = (int)(vf);


    /* ----------- CONTROLER CODE HERE ------------ */
    e_pos = goal_position - pos;
    if (abs(e_pos) < goal_tolerance) {
      e_pos = 0;
    }
        // P-term
    uP = Kp * (float)e_pos;

    // I-term
    uI = uI + Ki * (float)e_pos;

    // D-term
    uD = Kd * (float)e_pos - Kd * (float)prev_e_pos;

    // PI output (in Volts)
    u = uP + uI + uD;

    // Direction+
    if (u >= 0) {
      digitalWrite(IN_A, LOW);
      digitalWrite(IN_B, HIGH);
    } else {
      digitalWrite(IN_A, HIGH);
      digitalWrite(IN_B, LOW);
    }

    /* -------------------------------------------- */
    //OCR0B = af * exp(bf * u) + cf * exp(df * u); //Malaka linearization
    OCR0B = (uint8_t)abs(u);
    // Filter variables
    prev_pos = pos;
    prev_vf3 = prev_vf2;
    prev_vf2 = prev_vf1;
    prev_vf1 = prev_vf;
    prev_vf = vf;
    prev_vel = vel;
    prev_e_pos = e_pos;

    // PRINT VALUES TO PLOTTER
    Serial.print(pos); Serial.print('\t'); Serial.println(goal_position);
    
    // End of control loop, wait for the next timer2 interupt.
    control_loop = false;
  }

}




void position_update(int &pos) {
  uint8_t u8data;  uint32_t u32result;
  // Pulse to initiate new transfer
  digitalWrite(10, HIGH);
  digitalWrite(10, LOW);
  //Receive the 3 bytes (AS5145 sends 18bit word)
  for (uint8_t byteCount = 0; byteCount < 3; byteCount++) {
    u32result <<= 8;  // left shift the result so far - first time shifts 0's-no change
    SPDR = 0xFF;  // send 0xFF as dummy (triggers the transfer)
    while ( (SPSR & (1 << SPIF)) == 0);  // wait until transfer complete
    u8data = SPDR;  // read data from SPI register
    u32result |= u8data;  //store the byte
  }
  // TODO!  Check the flags before continue
  u32result >>= 12;
  int *ssi_pnt16 = (int *)&u32result;
  // POSITION without the offset
  pos = *ssi_pnt16 - 4096;
  // POSITION with the offset
  if (pos < offset) {
    pos = 4096 + (pos-offset);
  }else {
    pos = pos - offset;
  }
}

int velocity_update(int pos, int prev_pos) {
  if ((pos - prev_pos) < -400) {
    pos = pos + 4096;
  } else if ((pos - prev_pos) > 3700) {
    prev_pos = prev_pos + 4096;
  }
  return (pos - prev_pos);
}
